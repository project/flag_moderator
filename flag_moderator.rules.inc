<?php

/**
 * @file
 * Custom rules actions for the forum.
 */

/**
 * Implementation of hook_rules_event_info().
 */
function flag_moderator_rules_event_info() {
  return array(
    'flag_moderator_rules_event_closeflags_comment' => array(
      'label' => t('All open flags were closed on a reported comment.'),
      'module' => 'Flag Moderator',
      'arguments' => array(
        'comment' => array('type' => 'comment', 'label' => t('Flagged comment')),
        'message' => array('type' => 'string', 'label' => t('Closing message')),
      ),
      'eval input' => array('sender', 'from', 'to', 'cc', 'bcc', 'subject', 'message_html', 'message_plaintext', 'attachments'),
    ),
    'flag_moderator_rules_event_closeflags_node' => array(
      'label' => t('All open flags were closed on a reported node.'),
      'module' => 'Flag Moderator',
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('Flagged node')),
        'message' => array('type' => 'string', 'label' => t('Closing message')),
      ),
      'eval input' => array('sender', 'from', 'to', 'cc', 'bcc', 'subject', 'message_html', 'message_plaintext', 'attachments'),
    ),
  );
}
