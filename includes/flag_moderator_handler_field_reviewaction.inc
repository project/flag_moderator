<?php

/**
 * Views field handler for the operations links.
 *
 * @ingroup views
 */
class flag_moderator_handler_field_reviewaction extends views_handler_field {
  /**
   * Returns the flag object associated with our field.
   *
   * A field is in some relationship. This function reaches out for this
   * relationship and reads its 'flag' option, which holds the flag name.
   */
  function get_flag() {
    // When editing a view it's possible to delete the relationship (either by
    // error or to later recreate it), so we have to guard against a missing
    // one.
    if (isset($this->view->relationship[$this->options['relationship']])) {
      return $this->view->relationship[$this->options['relationship']]->get_flag();
    }
  }

  /**
   * Return the the relationship we're linked to. That is, the alias for its
   * table (which is suitbale for use with the various methods of the 'query'
   * object).
   */
  function get_parent_relationship() {
    $parent = $this->view->relationship[$this->options['relationship']]->options['relationship'];
    if (!$parent || $parent == 'none') {
      return NULL; // Base query table.
    }
    else {
      return $this->view->relationship[$parent]->alias;
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_text'] = array('default' => t('moderate'));
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#default_value' => $this->options['link_text'],
    );
  }
  
  function query() {
    $flag = $this->get_flag();
    $info = $flag->get_views_info();
    $parent = $this->get_parent_relationship();
    $left_table = $this->view->relationship[$this->options['relationship']]->table_alias;
    $this->aliases['content_id'] = $this->query->add_field($left_table, $info['join field']);
  }

  function render($values) {
    $content_id = $values->{$this->aliases['content_id']};
    $flag = $this->get_flag();
    $content_type = $flag->content_type;
    $flag_type = $flag->name;  
    $link_text = $this->options['link_text'];    
    return l($link_text, sprintf('admin/content/flags/%s/%s/%s', $flag_type, $content_type, $content_id));
  }
}
