<?php

/**
 * Implements hook_views_handlers().
 */
function flag_moderator_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'flag_moderator') . '/includes',
    ),
    'handlers' => array(
      'flag_moderator_handler_field_reviewaction' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}


/**
 * Implements hook_views_data_alter().
 */
function flag_moderator_views_data_alter(&$data) {
  $data['flag_content']['review'] = array(
    'title' => t('Moderator link'),
    'help' => t('Display link to view flags for this content.'),
    'field' => array(
      'handler' => 'flag_moderator_handler_field_reviewaction',
    ),
  );
}
